import React, { Component } from 'react';
import './App.css';
import NavBar from './components/NavBar/NavBar';

class App extends Component {

  render() {
    return (
      <div id="content-flex-fix">
        <div id="content-wrapper">
          <NavBar />
        </div>
    </div>
    );
  }
}

export default App;
