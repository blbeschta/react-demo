import React, { Component } from 'react';
import {PageHeader} from 'react-bootstrap';
import moment from 'moment';

class Home extends React.Component {

  state = {
    currentTime: moment().format('MMMM Do YYYY, h:mm a'),
    name: '',
  }

  handleClick = () => {
    this.setState({name: this.state.name ? '' : 'Marc'});
  }

  renderPageHeader = () => {
    return (
      <PageHeader className="text-center">Home Page</PageHeader>
    );
  }

  renderHomeContent = () => {
    return (
      <div>
        <h3 className="text-center text-success">Today's Date is {this.state.currentTime}</h3>
        <button className="btn btn-default" onClick={this.handleClick}>Click me</button>

        {this.state.name}
      </div>
    );
  }

  render() {
    return (
      <div>
      { this.renderPageHeader() }
      { this.renderHomeContent() }
      </div>
    );
  }

}

export default Home;
