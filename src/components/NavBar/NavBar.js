import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import {Nav, Navbar, NavItem} from 'react-bootstrap';

import Home from '../Home/Home';

class NavBar extends React.Component {

  renderNavBar = () => {

    const About = () => <h2 className="text-center">About</h2>;
    const Users = () => <h2 className="text-right text-danger">Users</h2>;

    return (
      <Router>
        <div>
          <Navbar>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="/">React-Bootstrap</a>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
              <NavItem eventKey={1} href="/link/">
                Link #1
              </NavItem>
              <NavItem eventKey={2} href="/link2/">
                Link #2
              </NavItem>
            </Nav>
          </Navbar>
          <Route path="/" exact component={Home} name={true} />
          <Route path="/link/" component={About} />
          <Route path="/link2/" component={Users} />
        </div>
      </Router>
    );
  }


  render() {
    return (
      <div>
        { this.renderNavBar() }
      </div>
    );
  }
}


export default NavBar;
